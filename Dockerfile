FROM python:3
ENV PYTHONUNBUFFERED=1
WORKDIR /code
RUN apt update && apt install -y --no-install-recommends \
        libgdal-dev \
        gcc && \
pip install numpy==1.18.1 && \
pip install gdal==2.4.0 && \
        rm -rf /var/lib/apt/lists/*
COPY requirements.txt /code/
RUN pip install -r requirementsForDocker.txt
COPY . /code/