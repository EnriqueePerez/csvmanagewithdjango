# CSV Management with Django

## Summary

App made for manage csv files. The route **/api/v1/datasets** allows you to send a csv file and save it into the PostgreSQL, later you can watch the files that you upload and when did you do it. 

Also you can watch the content of your csv file with the route **/api/v1/rows/.** Here you can look for your content file by ID.

Lastly, you can watch the users information that consume the previous endpoints in the route **/api/v1/logs.** This logger will show you the IP address of the user, the date of the request and from where did the API was consumed.

## Deployment

The app was deployed in a EC2 instance using docker compose. The instance is a Ubuntu 20.04 VM.

## URLs

[Datasets Management](http://3.142.241.62/api/v1/datasets/)

[Rows Viewer](http://3.142.241.62/api/v1/rows/)

[Users Logger](http://3.142.241.62/api/v1/logs/)

## Technologies

- Docker ⇒ Used for create the DB and Django instance
- Django ⇒ Used for the API and the Frontend
- PostgreSQL ⇒ Used to persist the data
