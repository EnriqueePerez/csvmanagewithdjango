from django.shortcuts import render
from django.views import View
from django.core.paginator import Paginator

from api.forms import DatasetForm
from django.contrib.gis.geos import Point
from .models import Dataset, Row, Logs

import csv
from io import TextIOWrapper
import datetime


# Function to get ip address
def get_client_ip(request):
    x_forwarded_for = request.META.get('HTTP_X_FORWARDED_FOR')
    if x_forwarded_for:
        ip = x_forwarded_for.split(',')[0]
    else:
        ip = request.META.get('REMOTE_ADDR')
    return ip


def get_user_info(request):
    # Saving the user info
    ip = get_client_ip(request)
    date = datetime.datetime.now()
    user = request.META['HTTP_USER_AGENT']
    user_data = Logs(ip_address=ip, date=date, user=user)
    user_data.save()


# Create your views here.
class ManageDatasets(View):
    form_class = DatasetForm
    template_name = 'api/manage_datasets.html'

    # GET REQUEST
    def get(self, request):
        get_user_info(request)
        form = DatasetForm()
        data = Dataset.objects.all().order_by('id')

        # Adding paginator
        paginator = Paginator(data, 5)
        page_number = request.GET.get('page')
        page_obj = paginator.get_page(page_number)

        return render(request, self.template_name, {'form': form, 'data': page_obj})

    # POST REQUEST
    def post(self, request):
        form = self.form_class(request.POST, request.FILES)
        if form.is_valid():
            print(form.cleaned_data)
            print('Dataset Form Validated Correctly')

            # Saving the file name
            new_entry = Dataset(name=request.POST['name'])
            new_entry.save()

            # Getting the inserted id
            inserted_id = new_entry.id
            print('id inserted', inserted_id)

            # Saving the content of the file
            file = TextIOWrapper(request.FILES['file'].file, encoding='UTF8')  # Converting binary file into text file
            data = csv.DictReader(file)
            next(data)  # Skipping the headers
            for row in data:
                new_row = Row(point=Point([float(row['latitude']), float(row['longitude'])]), dataset_id=new_entry,
                              client_id=int(row['client_id']), client_name=row['client_name'])
                new_row.save()
                print('imprimiendo row', row)

        return self.get(request)


class ManageRows(View):
    template_name = 'api/manage_rows.html'

    def get(self, request):
        if request.GET.get('dataset_id') is None:
            return render(request, self.template_name)
        else:
            get_user_info(request)

            # Sending the information
            dataset_id = int(request.GET.get('dataset_id'))
            data = Row.objects.filter(dataset_id=dataset_id).order_by('id')
            return render(request, self.template_name, {'data': data})


class ShowLogs(View):
    template_name = 'api/show_logs.html'

    def get(self, request):
        # Getting the logs
        data = Logs.objects.all().order_by('id')

        # Adding paginator
        paginator = Paginator(data, 5)
        page_number = request.GET.get('page')
        page_obj = paginator.get_page(page_number)

        return render(request, self.template_name, {'data': page_obj})
