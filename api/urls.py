from django.urls import path

from . import views

urlpatterns = [
    # /api/v1/datasets
    path('v1/datasets/', views.ManageDatasets.as_view(), name='ManageDatasets'),
    # /api/v1/rows
    path('v1/rows/', views.ManageRows.as_view(), name='ManageRows'),
    # /api/v1/logs
    path('v1/logs/', views.ShowLogs.as_view(), name='ShowLogs')

]
