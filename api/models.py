from django.contrib.gis.db import models


# Create your models here.
class Dataset(models.Model):
    name = models.CharField(max_length=95)
    date = models.DateTimeField(auto_now_add=True)


class Row(models.Model):
    dataset_id = models.ForeignKey('Dataset', on_delete=models.CASCADE)
    point = models.PointField()
    client_id = models.IntegerField()
    client_name = models.CharField(max_length=45)


class Logs(models.Model):
    ip_address = models.CharField(max_length=30)
    date = models.DateTimeField(auto_now_add=True)
    user = models.TextField()
