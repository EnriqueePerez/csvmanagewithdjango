from django import forms


class DatasetForm(forms.Form):
    name = forms.CharField(label='Name of the file', max_length=95, required=True)
    file = forms.FileField(allow_empty_file=False, required=True)
